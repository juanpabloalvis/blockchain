var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  //res.send('Crud get');
  console.log('CrudGet  ......sending a file '+__dirname)
  // Note: __dirname is directory that contains the JavaScript source code. Try logging it and see what you get!
  res.sendFile(__dirname + '/getForm.html')
});

module.exports = router;
